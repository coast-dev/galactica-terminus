# -*- coding: utf-8 -*-
# This file is part of the 'Terminus' Python package.
#
# Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
#
#  FREE SOFTWARE LICENCING
#  -----------------------
# This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
# software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
# CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
# and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
# and the software's author, the holder of the economic rights, and the successive licensors have only limited
# liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
# and/or developing or reproducing the software by the user in light of its specific status of free software, that may
# mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
# experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
# software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
# to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
# you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
#
#
# COMMERCIAL SOFTWARE LICENCING
# -----------------------------
# You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
# negotiate a specific contract with a legal representative of CEA.
#

import pytest
from unittest import mock
import os
import sys
import json
from Terminus.__main__ import datasource_dir_config
from Terminus.commons import TerminusCommons
import tempfile


class TestDatasourceCfg(object):

    def _build_test_json_file(self, tmpdir, cat, proj, simu, link_dict):
        """
        Build dummy simulation data directory hierarchy in temporary dict and write the corresponding JSON file to test
        datasource directory config function.

        :param tmpdir: temporary directory path
        :param cat: project category alias
        :param proj: project alias
        :param simu: simulation alias
        :param link_dict: symbolic link dictionary containing target data directories as values.
        :return: JSON filepath
        """
        # Create dummy data dirs
        for td in link_dict.values():
            os.makedirs(os.path.join(tmpdir, td), exist_ok=True)  # No exception raised if it already exists

        # Create JSON test dict
        d = {"category": cat, "project": {"alias": proj, "created": False}, "children": [
            {"alias": simu, "created": False, "children": [
                {"data_reference": k, "directory_path": os.path.join(tmpdir, v)} for k, v in link_dict.items()]}, ]
             }
        # Save test JSON file
        with tempfile.NamedTemporaryFile(prefix="test_datasource_cfg_", suffix=".json", dir=tmpdir, mode="w",
                                         delete=False) as jf:
            json.dump(d, jf)
            json_fpath = jf.name

        # Return temporary JSON file path
        return json_fpath

    def test_01_datasource_cfg(self):
        """
        Tests datasource director path definition CLI tool
        """
        # curr_dir = os.path.dirname(__file__)
        # test_json_file = os.path.join(curr_dir, 'datasource_directory_defs.json')
        with tempfile.TemporaryDirectory(prefix="test_datasource_") as tmpdir:
            # Create a dummy directory hierarchy to deploy snapshot data and test symlink creation
            cat = "STAR_FORM"
            proj = "MY_PROJ"
            simu = "SIMU_ONE"
            link_dict = {"ref_0015": "my_user/proj1/simu_A_cfg3/outputs/output_00015",
                         "ref_0035": "my_user/proj1/simu_A_cfg3/outputs/output_00035"}

            json_fpath = self._build_test_json_file(tmpdir, cat, proj, simu, link_dict)
            # Set Terminus data directory to temporary dir. and apply datasource config on JSON file (test mode)
            os.environ[TerminusCommons.ENVVAR_DATA_DIR] = tmpdir
            # Patch the builtin input() method so it returns 'n' as an answer
            with mock.patch('builtins.input', return_value="n"):
                datasource_dir_config(args=[json_fpath, ], test_mode=True)

            # Test directory /symlink creation and check symlink targets
            assert os.path.isdir(os.path.join(tmpdir, cat, proj, simu))
            for target in link_dict.values():
                lnk_name = os.path.join(tmpdir, cat, proj, simu, os.path.basename(target))
                assert os.path.islink(lnk_name)  # Symlink exists
                assert(os.readlink(lnk_name) == os.path.join(tmpdir, target))  # Symlink points to right target

    def test_datasource_cfg_undef_proj(self):
        """
        Tests datasource director path definition CLI tool
        """
        with tempfile.TemporaryDirectory(prefix="test_datasource_") as tmpdir:
            # Create a dummy directory hierarchy to deploy snapshot data and test symlink creation
            cat = "STAR_FORM"
            proj = ""  # Empty project alias => should yield error
            simu = "SIMU_TWO"
            link_dict = {"ref_0001": "my_user/proj1/simu_A_cfg3/outputs/output_00001", }

            json_fpath = self._build_test_json_file(tmpdir, cat, proj, simu, link_dict)
            # Set Terminus data directory to temporary dir. and apply datasource config on JSON file (test mode)
            os.environ[TerminusCommons.ENVVAR_DATA_DIR] = tmpdir
            with pytest.raises(ValueError, match="Project alias is not defined."):
                datasource_dir_config(args=[json_fpath, ], test_mode=True)

    def test_datasource_cfg_undef_cat(self):
        """
        Tests datasource director path definition CLI tool
        """
        with tempfile.TemporaryDirectory(prefix="test_datasource_") as tmpdir:
            # Create a dummy directory hierarchy to deploy snapshot data and test symlink creation
            cat = ""  # Empty category alias => should yield error
            proj = "PROJ_1"
            simu = "SIMU_TWO"
            link_dict = {"ref_0001": "my_user/proj1/simu_A_cfg3/outputs/output_00001", }

            json_fpath = self._build_test_json_file(tmpdir, cat, proj, simu, link_dict)
            # Set Terminus data directory to temporary dir. and apply datasource config on JSON file (test mode)
            os.environ[TerminusCommons.ENVVAR_DATA_DIR] = tmpdir
            with pytest.raises(ValueError, match="Undefined project category."):
                datasource_dir_config(args=[json_fpath, ], test_mode=True)


__all__ = ["TestDatasourceCfg"]


.. Terminus documentation master file, created by sphinx-quickstart on Fri Oct  2 15:02:05 2020.
   This file is part of the 'gal-terminus' Python package.
   -----------------------------------------------------------------------------------
   Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
   -----------------------------------------------------------------------------------
   -----------------------
   FREE SOFTWARE LICENCING
   -----------------------
   This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
   software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
   CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
   and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
   and the software's author, the holder of the economic rights, and the successive licensors have only limited
   liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
   and/or developing or reproducing the software by the user in light of its specific status of free software, that may
   mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
   experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
   software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
   to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
   you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
   -----------------------
   COMMERCIAL SOFTWARE LICENCING
   -----------------------------
   You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
   negotiate a specific contract with a legal representative of CEA.

Terminus package documentation
==============================

.. image:: https://img.shields.io/pypi/v/galactica-terminus?color=red&logo=PyPI
   :alt: PyPI - Version

.. image:: https://img.shields.io/pypi/status/galactica-terminus?color=orange&logo=PyPI
   :alt: PyPI - Status

.. image:: https://img.shields.io/pypi/pyversions/galactica-terminus?color=green&logo=PyPI
   :alt: PyPI - Python Version

.. image:: https://img.shields.io/readthedocs/galactica-terminus?logo=read-the-docs&logoColor=white
   :target: https://galactica-terminus.readthedocs.io/en/latest/?badge=latest
   :alt: Read the Docs - Documentation Status

.. image:: https://img.shields.io/pypi/wheel/galactica-terminus?label=wheel&color=yellowgreen&logo=PyPI
   :alt: PyPI - Wheel

.. image:: https://img.shields.io/pypi/l/galactica-terminus?color=informational&logo=PyPI
   :alt: PyPI - License


Introduction
------------

The purpose of the ``galactica-terminus`` Python package is to provide computational astrophysicists a way to give
access to their numerical simulation (raw) data via custom post-processing services made available to the community through
the `Galactica simulation database`_ web application.

This package lets you configure a ``Terminus`` deamon on a machine with computational resources you want to allocate to
the dedicated task of simulation data post-processing (data extraction, analysis, visualization, etc.). It is based on
the `Celery`_ Python asynchronous task management framework, using `RabbitMQ`_ as a message broker to communicate with
the `Galactica simulation database`_.


.. |celery_img| image:: _static/img/celery.png
   :height: 80
   :target: `Celery`_

.. |rabbitmq_img| image:: _static/img/logo-rabbitmq.png
   :height: 80
   :target: `RabbitMQ`_


|celery_img|  +   |rabbitmq_img|


.. _RabbitMQ: https://www.rabbitmq.com/
.. _Celery: https://docs.celeryproject.org
.. _Galactica simulation database: http://www.galactica-simulations.eu


.. toctree::
   :maxdepth: 1
   :caption: Documentation

   usage/installation
   usage/access
   usage/setup
   usage/daemon
   usage/process_simu_data
   usage/faq
   usage/example


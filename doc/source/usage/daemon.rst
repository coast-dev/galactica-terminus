.. Terminus documentation master file, created by sphinx-quickstart on Fri Oct  2 15:02:05 2020.
   This file is part of the 'gal-terminus' Python package.
   -----------------------------------------------------------------------------------
   Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
   -----------------------------------------------------------------------------------
   -----------------------
   FREE SOFTWARE LICENCING
   -----------------------
   This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
   software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
   CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
   and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
   and the software's author, the holder of the economic rights, and the successive licensors have only limited
   liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
   and/or developing or reproducing the software by the user in light of its specific status of free software, that may
   mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
   experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
   software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
   to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
   you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
   -----------------------
   COMMERCIAL SOFTWARE LICENCING
   -----------------------------
   You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
   negotiate a specific contract with a legal representative of CEA.

.. _usage_daemon:

Running Terminus server
=======================

.. _running_as_systemd_service:

How to launch Terminus workers as systemd deamon ?
--------------------------------------------------

The recommended setup is to run the *Terminus* server as a Systemd service :

.. code-block:: bash

    > sudo systemctl enable ${HOME}/.terminus/terminus.service
        Created symlink from /etc/systemd/system/multi-user.target.wants/terminus.service to /home/user1/.terminus/terminus.service.
        Created symlink from /etc/systemd/system/terminus.service to /home/user1/.terminus/terminus.service.

    > sudo systemctl start terminus.service
    > sudo systemctl status terminus.service
        ● terminus.service
        Loaded: loaded (/home/user1/.terminus/terminus.service.; enabled; vendor preset: enabled)
        Active: active (running) since Tue 2018-12-18 12:48:07 UTC; 6s ago
        Process: 3346 ExecStop=/bin/sh -c ${CELERY_BIN} multi stopwait ${CELERYD_NODES}    --pidfile=${CELERYD_PID_FILE} (code=exited, status=0/SUCCESS)
        Process: 3781 ExecStart=/bin/sh -c ${CELERY_BIN} multi start ${CELERYD_NODES}    -A ${CELERY_APP} --pidfile=${CELERYD_PID_FILE}    --logfile=${CELERYD_LOG_FILE} -loglevel=${CELERYD_LOG_LEVEL} ${CELERYD_OPTS} (
        Main PID: 3794 (python2)
            Tasks: 5
        Memory: 121.0M
            CPU: 972ms
        CGroup: /system.slice/terminus.service
                ├─3794 /home/user1/Terminus/python_env_1.0.0/bin/python2 -m celery worker --time-limit=300 -A Terminus --concurrency=2 --loglevel=INFO --logfile=/home/user1/.terminus/logs/w1.log --pidfile=/home/user1/.terminus/pids/w1.pid
                ├─3799 /home/user1/Terminus/python_env_1.0.0/bin/python2 -m celery worker --time-limit=300 -A webapp --concurrency=2 --loglevel=INFO --logfile=/home/user1/.terminus/logs/w1.log --pidfile=/home/user1/.terminus/pids/w1.pid
                └─3802 /home/user1/Terminus/python_env_1.0.0/bin/python2 -m celery worker --time-limit=300 -A webapp --concurrency=2 --loglevel=INFO --logfile=/home/user1/.terminus/logs/w1.log --pidfile=/home/user1/.terminus/pids/w1.pid

        Jan 16 12:48:07 galactica-private systemd[1]: Stopped celery.service.
        Jan 16 12:48:07 galactica-private systemd[1]: Starting celery.service...
        Jan 16 12:48:07 galactica-private sh[3781]: celery multi v4.1.1 (latentcall)
        Jan 16 12:48:07 galactica-private sh[3781]: > Starting nodes...
        Jan 16 12:48:07 galactica-private sh[3781]:         > w1@galactica-private: OK
        Jan 16 12:48:07 galactica-private systemd[1]: Started celery.service.

As you can, see a log file for your worker can be found under the ``logs`` directory at ``${HOME}/.terminus`` so you have a trace of what is happening with the workers.

.. _running_as_screen_process:

How to launch Terminus workers in *screen mode* ?
-------------------------------------------------

As an alternative, or for debugging purposes, you can also run the *Terminus* server in *screen* mode. In your case, you
need to change *workstation* by your defined ``hostname`` :

.. code-block:: bash

    > celery -A Terminus worker -c 4 -c:2 1 -Q workstation.terminus_jobs -Q:2 workstation.monitor -l INFO -n worker1@%n monitor@%n

        -------------- worker1@workstation v4.1.1 (latentcall)
        ---- **** -----
        --- * ***  * -- Linux-3.10.0-514.21.1.el7.x86_64-x86_64-with-centos-7.3.1611-Core 2018-10-02 14:37:15
        -- * - **** ---
        - ** ---------- [config]
        - ** ---------- .> app:         Terminus:0x7fc90d7981d0
        - ** ---------- .> transport:   amqp://workstation_terminus:**@123.456.789.123:9876/a_virtual_host
        - ** ---------- .> results:     disabled://
        - *** --- * --- .> concurrency: 4 (prefork)
        -- ******* ---- .> task events: OFF (enable -E to monitor tasks in this worker)
        --- ***** -----
        -------------- [queues]
                        .> workstation.terminus_jobs exchange=(direct) key=workstation.terminus_job


        [tasks]
        . execute_terminus_job
        . update_terminus_job_status

        [2018-10-02 14:37:15,911: INFO/MainProcess] Connected to amqp://workstation_terminus:**@**123.456.789.123:9876/a_virtual_host
        [2018-10-02 14:37:16,242: INFO/MainProcess] mingle: searching for neighbors
        [2018-10-02 14:37:17,715: INFO/MainProcess] mingle: sync with 2 nodes
        [2018-10-02 14:37:17,715: INFO/MainProcess] mingle: sync complete
        [2018-10-02 14:37:18,279: INFO/MainProcess] worker1@workstation ready.


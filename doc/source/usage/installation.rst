.. Terminus documentation master file, created by sphinx-quickstart on Fri Oct  2 15:02:05 2020.
   This file is part of the 'gal-terminus' Python package.
   -----------------------------------------------------------------------------------
   Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
   -----------------------------------------------------------------------------------
   -----------------------
   FREE SOFTWARE LICENCING
   -----------------------
   This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
   software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
   CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
   and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
   and the software's author, the holder of the economic rights, and the successive licensors have only limited
   liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
   and/or developing or reproducing the software by the user in light of its specific status of free software, that may
   mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
   experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
   software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
   to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
   you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
   -----------------------
   COMMERCIAL SOFTWARE LICENCING
   -----------------------------
   You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
   negotiate a specific contract with a legal representative of CEA.

.. _install:

Installation
============

Latest stable releases
----------------------

Using **pip**, you can easily download and install the latest version of the ``galactica-terminus`` package directly from the
`Python Package Index (PyPI)`_:

.. code-block:: bash

  > pip install galactica-terminus
    Collecting celery>=5.0.1
      Downloading celery-5.0.1-py3-none-any.whl (392 kB)
      |████████████████████████████████| 392 kB 18.2 MB/s 
    Collecting click-repl>=0.1.6
      Using cached click_repl-0.1.6-py3-none-any.whl (4.2 kB)
    Collecting click>=7.0
      Using cached click-7.1.2-py2.py3-none-any.whl (82 kB)
    Processing ./.cache/pip/wheels/03/a8/59/811a351d002c7fca47dc94c622f36a9bf6e27fa9da49c8ec0c/click_didyoumean-0.0.3-py3-none-any.whl
    Collecting billiard<4.0,>=3.6.3.0
      Using cached billiard-3.6.3.0-py3-none-any.whl (89 kB)
    Collecting vine<6.0,>=5.0.0
      Using cached vine-5.0.0-py2.py3-none-any.whl (9.4 kB)
    Collecting kombu<6.0,>=5.0.0
      Using cached kombu-5.0.2-py2.py3-none-any.whl (180 kB)
    Collecting pytz>dev
      Using cached pytz-2020.1-py2.py3-none-any.whl (510 kB)
    Collecting prompt-toolkit
      Using cached prompt_toolkit-3.0.8-py3-none-any.whl (355 kB)
    Collecting six
      Using cached six-1.15.0-py2.py3-none-any.whl (10 kB)
    Collecting importlib-metadata>=0.18; python_version < "3.8"
      Using cached importlib_metadata-2.0.0-py2.py3-none-any.whl (31 kB)
    Collecting amqp<6.0.0,>=5.0.0
      Using cached amqp-5.0.1-py2.py3-none-any.whl (46 kB)
    Collecting wcwidth
      Using cached wcwidth-0.2.5-py2.py3-none-any.whl (30 kB)
    Collecting zipp>=0.5
      Downloading zipp-3.4.0-py3-none-any.whl (5.2 kB)
    Installing collected packages: click, wcwidth, prompt-toolkit, six, click-repl, click-didyoumean, billiard, vine, zipp, importlib-metadata, amqp, kombu, pytz, celery, gal-terminus
    Successfully installed amqp-5.0.1 billiard-3.6.3.0 celery-5.0.1 click-7.1.2 click-didyoumean-0.0.3 click-repl-0.1.6 gal-terminus-0.5rc1 importlib-metadata-2.0.0 kombu-5.0.2
                           prompt-toolkit-3.0.8 pytz-2020.1 six-1.15.0 vine-5.0.0 wcwidth-0.2.5 zipp-3.4.0


.. _Python Package Index (PyPI): https://pypi.org/
.. Terminus documentation master file, created by sphinx-quickstart on Fri Oct  2 15:02:05 2020.
   This file is part of the 'gal-terminus' Python package.
   -----------------------------------------------------------------------------------
   Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
   -----------------------------------------------------------------------------------
   -----------------------
   FREE SOFTWARE LICENCING
   -----------------------
   This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
   software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
   CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
   and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
   and the software's author, the holder of the economic rights, and the successive licensors have only limited
   liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
   and/or developing or reproducing the software by the user in light of its specific status of free software, that may
   mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
   experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
   software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
   to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
   you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
   -----------------------
   COMMERCIAL SOFTWARE LICENCING
   -----------------------------
   You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
   negotiate a specific contract with a legal representative of CEA.

.. _galactica_access:

Galactica access setup
======================

In order to communicate with the `Galactica simulation database`_, your ``Terminus`` instance interacts with a messaging
server (based on the AMQP protocol) called `RabbitMQ`_. This messaging system allows the ``Terminus`` server to :

 - receive **job request messages** from the Galactica web app., containing information on :

   + which post-processing service to execute,
   + which simulation raw data to post-process,
   + what are the post-processing service parameter values requested by the user.

 - report back **job status update messages** to the Galactica web app., in order to :

   + display job status message on the user's personal `job requests` page,
   + send the user email notifications if his/her `job request` failed or succeeded.

Upon successful job completion, the produced data tarball needs to be uploaded (using a basic :command:`rsync` command) to
the Galactica server so that the user who submitted the job request can download it from the web application.

To configure these two ways of communication between your ``Terminus`` instance and the Galactica web app.:

 - **RabbitMQ messages** : see :ref:`get_rabbitmq_credentials`,
 - **rsync** : see :ref:`ssh_configuration`.


.. _get_rabbitmq_credentials:

Get RabbitMQ credentials
------------------------

To get RabbitMQ credentials, please kindly send an email to the Galactica database administrator
(admin@galactica-simulations.eu), providing the following information :

 - `Research institute` hosting the server on which you want to deploy Terminus,
 - Server ``hostname`` or a custom alias of your choice (see also ``terminus_host_name`` in :ref:`terminus_config`),
 - Server ``IP address``.

The Galactica database administrator will provide you the necessary parameters to setup ``Terminus``
(see :ref:`terminus_config`) :

    - a RabbitMQ user name
    - a RabbitMQ password
    - a RabbitMQ host
    - a RabbitMQ port
    - a RabbitMQ virtual host
    - a SSH target configuration (to add into your :file:`~/.ssh/config`).


.. note::
    You do not necessarily have to create an account on the `Galactica simulation database`_ to configure and run a
    ``Terminus``  server linked to the database. You only require `RabbitMQ`_ credentials and (obviously) system
    administration permissions on the machine.

.. _Galactica simulation database: http://www.galactica-simulations.eu
.. _RabbitMQ: https://www.rabbitmq.com/


.. _ssh_configuration:

SSH configuration
-----------------

Providing a SSH public key
^^^^^^^^^^^^^^^^^^^^^^^^^^

So ``Terminus`` can upload result tarballs from successful jobs, the user running the ``Terminus`` server instance need
to create a (passphrase-less) SSH key pair:

.. code-block:: console

    $ ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519_gal-terminus
        Generating public/private rsa key pair.
        Enter passphrase (empty for no passphrase): 
        Enter same passphrase again: 
        Your identification has been saved in ~/.ssh/id_ed25519_gal-terminus.
        Your public key has been saved in ~/.ssh/id_ed25519_gal-terminus.pub.
        The key fingerprint is:
        SHA256:6u8ZJ+5+5ne1gJ5M7Apb3K72RtPKn=Kgj74by3+zEp0 cluster_admin@hpc_clusterlab
        The key's randomart image is:
        +--[ED25519 256]--+
        |                 |
        |                 |
        |                 |
        |            E    |
        |        S....   .|
        |       ....o.. .+|
        |      .. o*..=.=o|
        |     . oO.--o.B+o|
        |     .=*=&@Osoo..|
        +----[SHA256]-----+

and forward the public key (the content of :file:`~/.ssh/id_ed25519_gal-terminus.pub`) by email to the Galactica database
administrator. Don't worry, it IS safe to communicate the public key by email.

.. _ssh_target_config:

Configuring SSH target for Galactica
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Configure a SSH target in your :file:`~/.ssh/config` file with the SSH key file you just created to be able to upload
data to the Galactica remote server:

.. code-block:: text

   Host Galactica_storage
   Hostname galactica_ip_addr
   User ssh_remote_user
   Port ssh_port
   IdentityFile ~/.ssh/id_ed25519_gal-terminus.pub

Here you can choose the target alias (here `Galactica_storage`) freely, you will need to provide it during the
:ref:`terminus_config` step. The Galactica server IP address (`galactica_ip_addr`), ssh user (here `ssh_remote_user`)
and SSH port (here `ssh_port`) will be provided to you by the Galactica database administrator, when he receives your
SSH public key.
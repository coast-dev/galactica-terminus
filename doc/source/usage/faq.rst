.. Terminus documentation master file, created by sphinx-quickstart on Fri Oct  2 15:02:05 2020.
   This file is part of the 'gal-terminus' Python package.
   -----------------------------------------------------------------------------------
   Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
   -----------------------------------------------------------------------------------
   -----------------------
   FREE SOFTWARE LICENCING
   -----------------------
   This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
   software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
   CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
   and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
   and the software's author, the holder of the economic rights, and the successive licensors have only limited
   liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
   and/or developing or reproducing the software by the user in light of its specific status of free software, that may
   mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
   experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
   software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
   to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
   you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
   -----------------------
   COMMERCIAL SOFTWARE LICENCING
   -----------------------------
   You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
   negotiate a specific contract with a legal representative of CEA.

FAQ
===

BUGS & SOLUTIONS
-----------------

Error with submission script
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Error ``no display name and no $DISPLAY`` with python scripts that use matplotlib:

   While running a python script that use Matplotlib and try to save a figure on a server it is
   mandatory to use a non-GUI backend for Matplotlib even just to save the figure. Indeed, an error or
   a warning can been raised or simply the output file will not be created but the script will end successfuly.
  
   .. code-block:: bash
    
      Error: no display name and no $DISPLAY environment variable.

   The solution is to add those lines at the top of the python script

   .. code-block:: python

       import matplotlib
       matplotlib.use("Agg")

   A *userwarning* will be raised

   .. code-block:: bash

       /home/user1/works/venv/local/lib/python2.7/site-packages/matplotlib/figure.py:457: UserWarning: matplotlib is currently using a non-GUI backend, so cannot show the figure
       "matplotlib is currently using a non-GUI backend, "


Error Terminus's daemonization
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Error ``-A option unrecognized`` with Celery daemonization:

.. code-block:: bash

  > journalctl -xe
    Error: no such option: -A 
    oct. 28 15:24:12 Workstation sh[23120]: celery multi v5.0.0 (singularity)
    oct. 28 15:24:12 Workstation sh[23120]: > Starting nodes...
    oct. 28 15:24:12 Workstation sh[23120]:         > celery1@workstation: * Child terminated with exit code 2
    oct. 28 15:24:12 Workstation sh[23120]: FAILED
    oct. 28 15:24:12 Workstation sh[23144]: > celery1@workstation: DOWN
    oct. 28 15:24:27 Workstation sh[23222]: Usage: __main__.py worker [OPTIONS]
    oct. 28 15:24:27 Workstation sh[23222]: Try '__main__.py worker --help' for help.
    oct. 28 15:24:27 Workstation sh[23222]: Error: no such option: -A
      
  This is a reported error in Celery 5.0.0, that has been fixed `Celery_issue_6363`_. Upgrade the celery to 5.0.1 fix that issue.

.. _Celery_issue_6363: https://github.com/celery/celery/issues/6363# 
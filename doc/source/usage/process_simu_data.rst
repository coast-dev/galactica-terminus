.. Terminus documentation master file, created by sphinx-quickstart on Fri Oct  2 15:02:05 2020.
   This file is part of the 'gal-terminus' Python package.
   -----------------------------------------------------------------------------------
   Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
   -----------------------------------------------------------------------------------
   -----------------------
   FREE SOFTWARE LICENCING
   -----------------------
   This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
   software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
   CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
   and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
   and the software's author, the holder of the economic rights, and the successive licensors have only limited
   liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
   and/or developing or reproducing the software by the user in light of its specific status of free software, that may
   mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
   experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
   software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
   to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
   you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
   -----------------------
   COMMERCIAL SOFTWARE LICENCING
   -----------------------------
   You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
   negotiate a specific contract with a legal representative of CEA.

.. _process_simu_data:

Simulation data processing
==========================


.. _how_to_add_simu_data:

How to add raw simulation data on your ``Terminus`` server ?
------------------------------------------------------------

The first step is now to add raw simulation data on the Terminus server filesystem. The first question that needs to be
answered is ``where ?``

To avoid any confusion, the directory hierarchy within the ``Terminus data directory`` must match perfectly the end of the URL
of the corresponding simulation page on Galactica, starting after ``galactica-simulations.eu/db/``. Since that url is unique,
it can be used to identify the target simulation data.

For example, the *ORION_FIL_MHD* simulation can be accessed at
``http://www.galactica-simulations.eu/db/STAR_FORM/ORION/ORION_FIL_MHD/``. Hence the part of that URL that is going to be
useful on Terminus is ``STAR_FORM/ORION/ORION_FIL_MHD/``.The top directory is the `project catagory alias` (Star formation), the
inner directory is the `projet alias` (Orion), and the child directory is the `simulation alias` (ORION_FIL_MHD).

To be accessed by Terminus data processing services, the raw data of that simulation must be stored within the
``Terminus data directory``, following the exact same path : ``${TERMINUS_DATA_DIR}/STAR_FORM/ORION/ORION_FIL_MHD/`` .
In this directory, you can transfer all the simulation snapshot directories (e.g. RAMSES outputs)
See :ref:`Example` for an additional example.

**New in version 0.7**

To help the Terminus server administrator configure properly all these raw simulation data directories under the
``Terminus data directory``, a ``terminus_datasource_cfg`` CLI tool is provided with the installation of Terminus.
Upon simulation snapshot pages creation (and data association to post-processing service) on the
`Galactica simulation database`_ web server (for projects hosted on your Terminus server), the Terminus server
administrator will be notified of new data path definitions to configure on the Terminus server filesystem.
It will take the form of a single JSON file that can be used as the only argument of the ``terminus_datasource_cfg``
command to automatically define the proper data paths and symbolic links :

.. code-block:: bash

    > terminus_datasource_cfg new_datasource.json
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Terminus datasource directory definitions ~~~~~~~~~~~~~~~~~~~~~~~~ #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    Project category directory '/data/Terminus/STAR_FORM' already exists.
     -> Project directory '/data/Terminus/STAR_FORM/ORION' already exists.
         * Simulation directory '/data/Terminus/STAR_FORM/ORION/ORION_FIL_MHD' already exists.
             - Created snapshot data symbolic link : '/data/Terminus/STAR_FORM/ORION/ORION_FIL_MHD/output_00050' => '/raid/data/Proj_ORION/fil/MHD/output_00050'.
             - Created snapshot data symbolic link : '/data/Terminus/STAR_FORM/ORION/ORION_FIL_MHD/output_00060' => '/raid/data/Proj_ORION/fil/MHD/output_00060'.
             - Created snapshot data symbolic link : '/data/Terminus/STAR_FORM/ORION/ORION_FIL_MHD/output_00080' => '/raid/data/Proj_ORION/fil/MHD/output_00080'.
             - Snapshot data symbolic link unchanged : '/data/Terminus/STAR_FORM/ORION/ORION_FIL_MHD/output_00120' => '/raid/data/Proj_ORION/fil/MHD/output_00120'.
             - Snapshot data symlink '/data/Terminus/STAR_FORM/ORION/ORION_FIL_MHD/output_00150' has changed:
                 + old target : '/raid/data/Proj_ORION/fil/Run4_phi3.45_beta0.5/output_00150'.
                 + new target : '/raid/data/Proj_ORION/fil/MHD/output_00150'.
               Overwrite (y='yes', n='no', a='yes to all', x='no to all') [Y] ? y
               -> Deleted deprecated snapshot data symbolic link : '/data/Terminus/STAR_FORM/ORION/ORION_FIL_MHD/output_00150' => '/raid/data/Proj_ORION/fil/Run4_phi3.45_beta0.5/output_00150'.
             - Created snapshot data symbolic link : '/data/Terminus/STAR_FORM/ORION/ORION_FIL_MHD/output_00150' => '/raid/data/Proj_ORION/fil/MHD/output_00150'.
    Done...
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #


Once this command is executed successfully, the Terminus can immediately access the configured simulation data, answer
Galactica incoming job request and execute data processing jobs. No Terminus server restart is necessary.

.. _Galactica simulation database: http://www.galactica-simulations.eu

.. _how_to_add_services:

How to create new post-processing services ?
--------------------------------------------

First, go to your defined ``terminus service directory`` and create a new directory matching the name of the service
(e.g. ``my_service``). In that directory, copy and paste the following python template for your new service in a new
python script, matching the name of the service (e.g. ``my_service.py``):

::

    ${HOME}/terminus
    └── services
        └── my_service
            ├── my_service.py
            └── service.json

.. literalinclude:: ../../../Terminus/etc/service_template.py
    :linenos:
    :language: python

Target data attributes
^^^^^^^^^^^^^^^^^^^^^^

In the above script, the ``run`` python method will be directly called by the Terminus job upon execution. The **data_path**
attribute is automatically transferred by Galactica during the job submission stage and matches the Galactica URL as
explained in the previous section.

The **data_ref** attribute is the unique identifier of the simulation snapshot, as defined in Galactica. It allows to distinguish
the various snapshots within a simulation and is transferred as a string (for example, the `data_ref` could be the
snapshot number to post-process). Both the **data_path** and the **data_ref** attributes are provided to help you find
the target data to run your service on (here in this example, they are parsed by the ``parse_data_ref`` function).

Test mode boolean attribute
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The last attribute **test** is mandatory (set to False by default) and is used by Galactica to periodically test
the Terminus data processing service. Indeed, Galactica will schedule periodic job requests in test mode (test=True) to
monitor the availability of the Terminus server, the consistency of the data processing services, the existence of
the target data and the correct execution of the processing service.

In `test` mode, the Terminus service will be run with the default values for the custom attributes (see next section),
Galactica will not provide any value for these attributes. For performance reasons, you are advised to choose
default values so that your service runs in the most degraded way (e.g. lowest resolution, small geometric region, ...)
with these default attribute values.


Custom service attributes
^^^^^^^^^^^^^^^^^^^^^^^^^

The attributes between **data_ref** and **test** are specific to your data processing service and can be customized at
will. The values for these attributes will be defined by the requesting user in the job submission form online.
In this template we use only 3 attributes (argument_1, argument_2, argument_3) but you can use as many attributes as you
need to perform your service.

You are encouraged to restrict the set of values considered valid for each parameter (e.g. value range for numeric
attributes, limited set of possible resolution, restricted choice of physical quantity to process, etc.) and raise errors
in case the received values are invalid. You can notify the Galactica administrator of these restrictions in order to design
the job request submission form online accordingly.

The choice of the default values for your custom attributes is important: they must be set to run the service in a fast
and memory-friendly way (see test mode section above).


Where the service must write data  ?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

During its execution, the data processing service will run in a dedicated job directory created in the `Terminus job directory`
(see :ref:`terminus_config`) :

::

    ${HOME}/terminus
    └── jobs
        ├── 148_density_map
        |    └── out
        └── 156_datacube3D
             └── out

within this job execution directory, an ``out/`` directory has been created for you tu put all the data files you want your
service to provide. You only need to write data into this local ``out/`` directory within your service script and this
directory will be tarballed upon job completion and finally uploaded to the Galactica server.


Service runtime configuration file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In addition to the service script file, Terminus requires a small JSON file named ``service.json`` that must contain the
job **type** and **interpreter** required to run the service script. This allows, for example, the user to use python 2
service script while the Terminus server itself uses a python 3 environment, or even separate the python environments for
each data processing service (each with their own dependencies).

Optionally, the job **n_nodes** (number of nodes), **n_cores** (total number of cores) and **timout_mn** (SLURM job timeout
in minutes) integer parameters can be set (if you configured the Terminus server to submit processing jobs as SLURM jobs).
These parameter values will define the SLURM submission parameters ``#SBATCH -N n_nodes``, ``#SBATCH -n n_cores`` and
``SBATCH -t timeout_duration``. If left undefined, the default number of node is 1, the default total number of cores is 8,
and the default SLURM job timeout 30 minutes.

The content of the ``service.json`` must look like this:

.. literalinclude:: ../../../Terminus/etc/python_service_template.json
  :language: JSON

Publish your new post-processing service on the Galactica web app
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once this is done, **the final step** for the service to be available on Galactica, is to send some information to a
Galactica admin :

 * the name of your service and the name of the Terminus server on which you deployed it,
 * a description of your data processing service, to document it on the Galactica web pages (full HTML content if you
   prefer, along with inserted images if required),
 * the custom attribute names of the run method as well as their type and restrictions on their valid values. For example,
   **argument_1** must be an integer in the range [0,10], **argument_2** a character that can only take one of those
   values *[x,y,z]*, **argument_3** must a string chosen in the set *['gas density', 'gas temperature', 'gas metallicity']*, etc.

Once defined by the Galactica admin on the web server, you will be able to connect this new data processing service to
your project so that authenticated users could submit job requests online to manipulate your raw data.

.. note::

   There is no need need to restart your ``Terminus`` server instance when :
     - you create a new post-processing service script into your ``terminus_service_directory``,
     - you modify an existing service script into your ``terminus_service_directory``,
     - you activate a new ``Terminus`` post-processing service on `Galactica`_.


.. _Galactica: http://www.galactica-simulations.eu
.. Terminus documentation master file, created by sphinx-quickstart on Fri Oct  2 15:02:05 2020.
   This file is part of the 'gal-terminus' Python package.
   -----------------------------------------------------------------------------------
   Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
   -----------------------------------------------------------------------------------
   -----------------------
   FREE SOFTWARE LICENCING
   -----------------------
   This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
   software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
   CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
   and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
   and the software's author, the holder of the economic rights, and the successive licensors have only limited
   liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
   and/or developing or reproducing the software by the user in light of its specific status of free software, that may
   mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
   experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
   software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
   to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
   you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
   -----------------------
   COMMERCIAL SOFTWARE LICENCING
   -----------------------------
   You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
   negotiate a specific contract with a legal representative of CEA.

.. _terminus_config:

Terminus configuration
======================

Once installed (see :ref:`install`), the ``Terminus`` package provides a helper command ``terminus_config`` designed to
help you configure your `Terminus server instance`. This command will prompt you to answer some questions, some of them
already answered in the previous section (see :ref:`galactica_access`), the others are detailed below.


.. warning:: Important
   The user who configures and runs the ``Terminus`` server **MUST** be the same user as the one who created the SSH key
   pair (see :ref:`ssh_configuration`). For security reasons, it should be an unprivileged user.


First time configuration
------------------------

The ``terminus_config`` command will create ``.terminus`` subdirectory (if not already present) into your ``${HOME}`` directory.
This directory will contain the required configuration information to run your ``Terminus`` server. This command will
guide you to define a few parameters (or hit ``Enter`` to use the default value) :

 - **terminus_host_name** : name of the host of the ``Terminus`` server instance. By default the `hostname` of the machine,
   but can be set to a user-defined machine alias (name you gave to the `Galactica`_ web app. administrator in the
   section :ref:`get_rabbitmq_credentials`).
 - **terminus_data_directory** : root directory path on the local filesystem where raw simulation data must be stored
   (see also :ref:`how_to_add_simu_data`).
 - **terminus_job_directory** : directory path on the local filesystem where `post-processing job data directories` will
   be stored. For each job request received by your ``Terminus`` server, a temporary directory will be created at this
   location :

    + Upon failure, the `job data directory` will be moved to the ``__failed`` subdirectory,
    + Upon completion, the `job data directory` will be compressed and uploaded to the `Galactica`_ database server before
      cleanup.

 - **terminus_service_directory** : directory path on the local filesystem where you must save all your custom
   post-processing service scripts (see also :ref:`how_to_add_services`).
 - **terminus_number_of_nodes** : number of `workers` (processes) will be run on your ``Terminus`` server. It is usually
   recommended to use **1** `worker` per machine for this use case but you can increase it.
 - **terminus_concurrency** : number of concurrent `threads` per `worker` that are running on your ``Terminus`` server
   instance. It depends on how much computational resources your are willing/authorised to allocate to simulation data
   post-processing through the `Galactica`_ web interface (you may discuss this particular topic with your head of lab.,
   the machine sysadmin and/or the person who funded its purchase). It is recommended to set this parameter at a value
   that is not higher than the total number of CPUs available on that machine. Keep in mind that the maximum total
   number of concurrent data post-processing jobs that could be run by the ``Terminus server`` is :

   :math:`N_{max} = {terminus}\_{number}\_{of}\_{nodes} \times {terminus}\_{concurrency}`

 - **Galactica server SSH target** : SSH target name defined in your local ~/.ssh/config file that will be used to upload
   post-processed data back to the web application filesystem via a secure rsync link (see :ref:`ssh_target_config`).
 - **SLURM_submission** : If SLURM is installed ont the machine then you can answer ``Y`` to this question and job
   submission will be done using the ``sbatch`` command. Otherwise answer ``n``.
 - The `RabbitMQ` credentials obtained previously (see :ref:`get_rabbitmq_credentials`) :

     + **RabbitMQ_user_name**, **RabbitMQ_host**, **RabbitMQ_port**, **RabbitMQ_virtual_host**. In addition, you need to
       edit the ``~/.terminus/_secret/rabbitmq_server_pwd`` to store the RabbitMQ user password in a secure way (file
       permissions are automatically set, you do not need to change them).


.. note::

   File path auto-completion, tilde expansion and environment variable are available while typing values during the
   execution of the ``terminus_config`` command to help you out.

Finally addition, ``terminus_config`` inform the user of the configured user and group. In the case of a fresh install,
the result of ``terminus_config`` command looks like:

.. code-block:: console

    $ terminus_config
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # ~~~~~~~~~~~~~~~~~~~~~~ Terminus server configuration helper ~~~~~~~~~~~~~~~~~~~~~~~ #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

            > Creating '/home/user1/.terminus' directory into the '/home/user1' directory.
            > Creating '_secret' directory into the '/home/user1/.terminus' directory.

    Configuration of Terminus
    Enter a terminus host name (current 'workstation'):
          > Default value used
    Enter a terminus data directory (current '/home/user1'): /home/user1/simulation_data
    Enter a terminus job directory (current '/home/user1'): /home/user1/terminus_jobs
    Enter a terminus service directory (current '/home/user1'): /home/user1/terminus_services
    Enter a terminus number of nodes (current 2): 1
    Enter a terminus number of concurrency/node (current 2): 4
    Enter a Galactica server SSH target name (configured in your local ~/.ssh/config file) (current 'Galactica_storage'):
          > Default value used
    Use SLURM for job submission ? (Y/n) (current N): n

    > Configured User: user1
    > Configured Group: user1

    ------------------------------------- RabbitMQ configuration -------------------------------------------
    Enter a rabbitmq username (current '__empty__')    : workstation_terminus
    Enter a rabbitmq host (current '__empty__')        : 192.168.1.1
    Enter a rabbitmq port (current '__empty__')        : 12541
    Enter a rabbitmq virtual host (current '__empty__'): a_virtual_host

    > Now set the provided RabbitMQ password in the file : /home/user1/.terminus/_secret/rabbitmq_server_pwd
    ---------------------------------- RabbitMQ configuration completed ------------------------------------

    Terminus configuration file written successfully.


The ``.terminus`` directory will contain the configuration files for Terminus :
 - ``terminus.env`` : file containing environment variable required to run the ``Terminus`` server,
 - ``terminus.service`` : systemd service config. file used for the deamonization of ``Terminus`` (see
   :ref:`running_as_systemd_service`),
 - ``_secret/rabbitmq_server_pwd`` : file where you must store the RabbitMQ server password.


Update an old configuration
---------------------------

In the case of a subsequent call to the ``terminus_config`` command, parameter values you had set previously will be
proposed to you as *default values* but your previous configuration will not be overridden before it is backuped :

.. code-block:: console

    $ terminus_config
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # ~~~~~~~~~~~~~~~~~~~~~~ Terminus server configuration helper ~~~~~~~~~~~~~~~~~~~~~~~ #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    Found a '.terminus' directory into the home directory

    > terminus.env configuration file found !
      > backup to : '/local/home/user1/.terminus/terminus.env.27-10-2020-14-11-56'

    > terminus.service configuration file found !
      > backup to : '/local/home/user1/.terminus/terminus.service.27-10-2020-14-11-56'

    Terminus configuration loaded, ready to update.

    Configuration of Terminus
    Enter a terminus host name (current 'workstation'):
          > Default value used
    Enter a terminus data directory (current '/home/user1/simulation_data'):
          > Default value used
    Enter a terminus job directory (current '/home/user1/terminus_jobs'): /home/user1/work_terminus/jobs
    Enter a terminus service directory (current '/home/user1/terminus_services'): /home/user1/work_terminus/services
    Enter a terminus number of nodes (current 1):
          > Default value used
    Enter a terminus number of concurrency/node (current 4):
          > Default value used
    Enter a Galactica server SSH target name (configured in your local ~/.ssh/config file) (current 'Galactica_storage'):
          > Default value used
    Use SLURM for job submission ? (Y/n) (current N): n

    > Configured User: user1
    > Configured Group: user1

    ...

.. _Galactica: http://www.galactica-simulations.eu

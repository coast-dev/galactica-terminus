Quick-start guide
=================

How to add data ?
-----------------

The first step is now to add simulation data. To do so, the PATH, starting from this location ``terminus data directory``, must be the same as the one displayed in the webpage URL on Galactica for the simulation.
For example, the *ORION_FIL_MHD* simulation is at http://www.galactica-simulations.eu/db/STAR_FORM/ORION/ORION_FIL_MHD/ so in the *ORION* project under the category *STAR_FORM*, therefore, the data path must be
``STAR_FORM/ORION/ORION_FIL_MHD`` starting from the configured ``terminus data directory``. See :ref:`Example` for an additional example.

How to create new services ? 
----------------------------

First, go to your defined ``terminus service directory`` and create a new directory. The name of the directory must match the name of the service.


Copy and paste the following python template for your new service :

.. literalinclude:: ../../../Terminus/etc/service_template.py
    :linenos:
    :language: python

In the above script, we use only 3 additionals arguments (argument_1, argument_2, argument_3) but you can use as much argument as you need to perform your service.

In addition, to this file. Terminus require a small file named ``python.info`` that must contains the python interpretor required to run the service script. This allow the user to use python 2 service script while in Terminus environment (python 3).

The content of the ``python.info`` must look like this:

.. code-block:: bash

    PYTHON=/path/to/python/env/pythonX.Y

Once this is done, **the final step** for the service to be available on Galactica, is to send the signature of the
run function to a Galactica admin as well as the type and the range for the extra parameters. For example, **argument_1**
must be an integer in the range [0,10], **argument_2** a character that can only take one of those values *[x,y,z]*, etc.

How to launch Terminus workers in *screen mode* ?
-------------------------------------------------

You may run the *Terminus* server in *screen* mode. In your case, you need to change *workstation* by your defined ``hostname`` :

.. code-block:: bash

    > celery -A Terminus worker -c 4 -Q workstation.terminus_jobs -l INFO -n worker1@%n
    
        -------------- worker1@workstation v4.1.1 (latentcall)
        ---- **** ----- 
        --- * ***  * -- Linux-3.10.0-514.21.1.el7.x86_64-x86_64-with-centos-7.3.1611-Core 2018-10-02 14:37:15
        -- * - **** --- 
        - ** ---------- [config]
        - ** ---------- .> app:         Terminus:0x7fc90d7981d0
        - ** ---------- .> transport:   amqp://workstation_terminus:**@123.456.789.123:9876/a_virtual_host
        - ** ---------- .> results:     disabled://
        - *** --- * --- .> concurrency: 4 (prefork)
        -- ******* ---- .> task events: OFF (enable -E to monitor tasks in this worker)
        --- ***** ----- 
        -------------- [queues]
                        .> workstation.terminus_jobs exchange=(direct) key=workstation.terminus_job
                        

        [tasks]
        . execute_terminus_job
        . update_terminus_job_status

        [2018-10-02 14:37:15,911: INFO/MainProcess] Connected to amqp://workstation_terminus:**@**123.456.789.123:9876/a_virtual_host
        [2018-10-02 14:37:16,242: INFO/MainProcess] mingle: searching for neighbors
        [2018-10-02 14:37:17,715: INFO/MainProcess] mingle: sync with 2 nodes
        [2018-10-02 14:37:17,715: INFO/MainProcess] mingle: sync complete
        [2018-10-02 14:37:18,279: INFO/MainProcess] worker1@workstation ready.

How to launch Terminus workers as systemd deamon ?
--------------------------------------------------

As an alternative, you can configure a systemd service for the **Terminus** server :

.. code-block:: bash

    > sudo systemctl enable ${HOME}/.terminus/terminus.service
        Created symlink from /etc/systemd/system/multi-user.target.wants/terminus.service to /home/user1/.terminus/terminus.service.
        Created symlink from /etc/systemd/system/terminus.service to /home/user1/.terminus/terminus.service.

    > sudo systemctl status terminus.service
        ● terminus.service
        Loaded: loaded (/home/user1/.terminus/terminus.service.; enabled; vendor preset: enabled)
        Active: active (running) since Tue 2018-12-18 12:48:07 UTC; 6s ago
        Process: 3346 ExecStop=/bin/sh -c ${CELERY_BIN} multi stopwait ${CELERYD_NODES}    --pidfile=${CELERYD_PID_FILE} (code=exited, status=0/SUCCESS)
        Process: 3781 ExecStart=/bin/sh -c ${CELERY_BIN} multi start ${CELERYD_NODES}    -A ${CELERY_APP} --pidfile=${CELERYD_PID_FILE}    --logfile=${CELERYD_LOG_FILE} -loglevel=${CELERYD_LOG_LEVEL} ${CELERYD_OPTS} (
        Main PID: 3794 (python2)
            Tasks: 5
        Memory: 121.0M
            CPU: 972ms
        CGroup: /system.slice/terminus.service
                ├─3794 /home/user1/Terminus/python_env_1.0.0/bin/python2 -m celery worker --time-limit=300 -A Terminus --concurrency=2 --loglevel=INFO --logfile=/home/user1/.terminus/logs/w1.log --pidfile=/home/user1/.terminus/pids/w1.pid
                ├─3799 /home/user1/Terminus/python_env_1.0.0/bin/python2 -m celery worker --time-limit=300 -A webapp --concurrency=2 --loglevel=INFO --logfile=/home/user1/.terminus/logs/w1.log --pidfile=/home/user1/.terminus/pids/w1.pid
                └─3802 /home/user1/Terminus/python_env_1.0.0/bin/python2 -m celery worker --time-limit=300 -A webapp --concurrency=2 --loglevel=INFO --logfile=/home/user1/.terminus/logs/w1.log --pidfile=/home/user1/.terminus/pids/w1.pid

        Jan 16 12:48:07 galactica-private systemd[1]: Stopped celery.service.
        Jan 16 12:48:07 galactica-private systemd[1]: Starting celery.service...
        Jan 16 12:48:07 galactica-private sh[3781]: celery multi v4.1.1 (latentcall)
        Jan 16 12:48:07 galactica-private sh[3781]: > Starting nodes...
        Jan 16 12:48:07 galactica-private sh[3781]:         > w1@galactica-private: OK
        Jan 16 12:48:07 galactica-private systemd[1]: Started celery.service.

As you can, see a log file for your worker can be found under the ``logs`` directory at ``${HOME}/.terminus`` so you have a trace of what is happening with the workers.
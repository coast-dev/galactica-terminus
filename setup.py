# -*- coding: utf-8 -*-
# This file is part of the Terminus Python package.
#
# Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
#
#  FREE SOFTWARE LICENCING
#  -----------------------
# This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
# software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
# CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
# and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
# and the software's author, the holder of the economic rights, and the successive licensors have only limited
# liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
# and/or developing or reproducing the software by the user in light of its specific status of free software, that may
# mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
# experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
# software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
# to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
# you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
#
#
# COMMERCIAL SOFTWARE LICENCING
# -----------------------------
# You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
# negotiate a specific contract with a legal representative of CEA.
#
from setuptools import setup, find_packages


setup(
    name="galactica-terminus",
    version="0.7",  # Do not forget to update it in Terminus/__init__.py and also in doc/source/conf.py
    install_requires=[
        'celery>=5.0.5',  # important to use >=5.0.1 because bug fix with daemonization
    ],
    python_requires='!=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, !=3.4.*, <4',  # 3.5/3.6/3.7/3.8/3.9
    packages=find_packages(include=['Terminus', 'Terminus.*']),
    include_package_data=True,

    # Metadata to display on PyPI
    author="Damien CHAPON ; Loic STRAFELLA",
    description="Celery-based async. task management daemon for astrophysical simulation data distributed post-processing",
    long_description="""The Terminus package is an astrophysical simulation data post-processing asynchronous
                     task management system. It is based on Celery 5 and compatible with Python 3.5+.
                     
                     It interacts with the Galactica simulations database (http://www.galactica-simulations.eu)
                     through a RabbitMQ message broker to provide raw simulation data distributed data processing
                     services to the scientific community.
                     """,
    author_email="loic.strafella@cea.fr",
    license="CEA CNRS Inria Logiciel Libre License, version 2.1 (CeCILL-2.1, http://www.cecill.info)",

    keywords="distributed processing, astrophysics, numerical simulations, asynchronous task, analysis, visualization",
    url="https://galactica-terminus.readthedocs.io",
    download_url="https://gitlab.com/coast-dev/galactica-terminus",
    project_urls={
        "Source": "https://drf-gitlab.cea.fr/coast-dev/terminus/",
    },

    classifiers=["Intended Audience :: Science/Research",
                 "License :: OSI Approved :: CEA CNRS Inria Logiciel Libre License, version 2.1 (CeCILL-2.1)",
                 "Operating System :: MacOS :: MacOS X",
                 "Operating System :: POSIX :: Linux",
                 # "Development Status :: 3 - Alpha",
                 # "Development Status :: 4 - Beta",
                 "Development Status :: 5 - Production/Stable",
                 "Programming Language :: Python :: 3.5",
                 "Programming Language :: Python :: 3.6",
                 "Programming Language :: Python :: 3.7",
                 "Programming Language :: Python :: 3.8",
                 "Programming Language :: Python :: 3.9",
                 "Topic :: Software Development :: Version Control :: Git",
                 "Topic :: Scientific/Engineering :: Physics",
                 "Topic :: Scientific/Engineering :: Astronomy",
                 "Topic :: Scientific/Engineering :: Information Analysis"],

    entry_points={
        "console_scripts": [
            "terminus_config = Terminus.__main__:server_config",
            "terminus_datasource_cfg = Terminus.__main__:datasource_dir_config"
        ]
    },

)

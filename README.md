# Terminus package

Celery-based async. task management daemon for astrophysical simulation data distributed post-processing through the [Galactica simulation database](http://www.galactica-simulations.eu).
Communications with the database are handled by a [RabbitMQ](https://www.rabbitmq.com/) message broker.

- PyPI package : [galactica-terminus](https://pypi.org/project/galactica-terminus/).
- Documentation : [Terminus documentation](https://galactica-terminus.readthedocs.io/en/latest/).
## REQUIREMENTS

    Terminus package is supported by Python version from 3.5 to 3.9. It requires:

    -celery >= 5.0.1 [celery](https://docs.celeryproject.org/en/v5.0.0/)

## INSTALLATION INSTRUCTIONS


## Release notes

#### Terminus version 0.7

 * Added new **terminus_datasource_cfg** CLI tool to help administrator symlink simulation data directories automatically, 
 * Added SLURM number of nodes/number of cores options in data-processing service JSON configuration file,
 * Added SLURM data-processing job timeout option in service JSON configuration file (#SBATCH argument).

#### Terminus version 0.6

 * Compatibility with Python 3.5/3.6/3.7/3.8/3.9 
 * Requires Celery >= v5.0.1
 * Added data-processing service and Terminus server status monitoring tasks (+ Celery queue)

#### Terminus version 0.5

 * Initial release with simple data processing job execution tasks Celery queues,
 * **terminus_config** CLI provided to help administrator configure the Terminus server deployment,
 * Valid IP address verification in configuration,
 * Creation of missing job/service/data directories, if missing.

    
## Developper notes

Galactica-terminus requires an extra package for the documentation : `sphinx-rtd-theme==0.5.0`

Package structure of `Galactica-terminus`:

 - **doc**  : contains sphinx generated doc of galactica-terminus
 - `LICENSE`
 - `README.md`
 - `setup.py`
 - **Terminus** : contains true Terminus Python package source files
     - `celery.py` : Celery application configuration and exchange/queues definition,
     - `tasks.py`: Celery Tasks definition (data-processing job + monitoring action)
     - `__main__.py` : `terminus_config` and `terminus_datasource_cfg` entrypoints definition (CLI tools),
     - `services_example` : example directory of data-processing service scripts,
     - `etc` : configuration file firectory
       * `terminus.env` : defines the necessary environment variables for the execution of the Terminus server,
       * `terminus.service` : Systemd service file handling properly the boot/shutdown/restart behaviour of the Terminus server,
       * `service_template.py` and `python_service_template.json` : template files to implement a new custom data-processing Python service.